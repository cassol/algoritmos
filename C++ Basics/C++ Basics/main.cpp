//
//  Jogo de adivinhação
//
// Created by Gavin Milroy
// Adaptado/traduzido por Vini Cassol on 28/08/19

#include <iostream>
#include <ctime>
using namespace std;

void ImprimeIntro(int Dificuldade)
{
    cout << "\n\nVoce eh um agente secreto preso em uma sala com nivel " << Dificuldade << " de seguranca...\nDescubra o codigo correto para prosseguir...\n\n";
}

bool PlayGame(int Dificuldade)
{
    ImprimeIntro(Dificuldade);
    
    // Gera o codigo secreto
    const int codA = rand() % Dificuldade + Dificuldade;
    const int codB = rand() % Dificuldade + Dificuldade;
    const int codC = rand() % Dificuldade + Dificuldade;
    
    const int soma = codA + codB + codC;
    const int produto = codA * codB * codC;
    
    
    // Print CodeSum and CodeProduct to the terminal
    cout << "+ O codigo possui tres numeros";
    cout << "\n+ A soma dos numeros eh " << soma;
    cout << "\n+ A multiplicacao dos numeros eh: " << produto << endl;
    
    // Guarda os palpites
    int valorA, valorB, valorC;
    cin >> valorA >> valorB >> valorC;
    
    int palpiteSoma = valorA + valorB + valorC;
    int palpiteProd = valorA * valorB * valorC;
    
    // Verifica o palpite
    if (palpiteSoma == soma && palpiteProd == produto)
    {
        cout << "\n*** Muito bem! Voce decifrou o codigo! Tente o proximo! ***";
        return true;
    }
    else
    {
        cout << "\n*** Codigo Invalido! Preste atencao! Tente novamente! ***";
        return false;
    }
}

int main()
{
    srand(time(NULL)); // utiliza o momento do dia para numeros aleatorios
    
    int dificuldade = 1;
    int const numNiveis = 2;
    
    while (dificuldade <= numNiveis) //O jogo roda ateh o jogador finalizar o ultimo nivel
    {
        bool bLevelComplete = PlayGame(dificuldade);

        
        if (bLevelComplete)
        {
            dificuldade++;
        }
    }
    cout << "\n*** Muito bem! Você liberou a saida, agora CORRA!!! ***\n";
    return 0;
}
